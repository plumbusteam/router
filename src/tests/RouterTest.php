<?php namespace Plumbus\tests;

use Plumbus\Router;

class RouterTest extends \PHPUnit_Framework_TestCase
{
    /**
     * @expectedException \Plumbus\Exception\Runtime
     */
    public function testRouterNotInitialized()
    {
        $router = new Router();
        $router->route('some/url');
    }

    /**
     * @expectedException \Plumbus\Exception\NotFound
     */
    public function testRouterThrows404()
    {
        $router = new Router();
        $router->setRoutingMap(['']);
        $router->route('some/url');
    }

    /**
     * @expectedException \Plumbus\Exception\Redirect
     */
    public function testRouterRedirectsTrailingSlash()
    {
        $router = new Router();
        $router->setRoutingMap(['']);
        try {
            $router->setTrailingSlashBehavior(
                Router::TRAILING_SLASH_BEHAVIOR_REDIRECT_IF_EXISTS
            );
            $router->route('some/url/');
        } catch (Redirect $exception) {
            $this->assertEquals('some/url', $exception->getRedirectUri());
            throw $exception;
        }
    }

    /**
     * @expectedException \Plumbus\Exception\Redirect
     */
    public function testRouterRedirectsNoTrailingSlash()
    {
        $router = new Router();
        $router->setRoutingMap(['']);
        try {
            $router->setTrailingSlashBehavior(
                Router::TRAILING_SLASH_BEHAVIOR_REDIRECT_IF_NOT_EXISTS
            );
            $router->route('some/url');
        } catch (Redirect $exception) {
            $this->assertEquals('some/url/', $exception->getRedirectUri());
            throw $exception;
        }
    }

    /**
     * @throws Redirect
     * @throws \Plumbus\Exception\NotFound
     */
    public function testRouterHitViaMask()
    {
        $router = new Router();
        $router->setTrailingSlashBehavior(
            Router::TRAILING_SLASH_BEHAVIOR_IGNORE
        );
        $router->setRoutingMap(
            [
                '' => 'index_page',
                '%d' => 'single_integer', // http://www.ru/100
                '%s' => 'single_string',// http://www.ru/any_text,
                'callback' => function () {
                    return 'HELLO';
                },
                'test' => [
                    '%s' => [
                        '' => 'test/string', // http://www.ru/test/any_text
                        '%d' => 'test/string/integer', // http://www.ru/test/any_text/any_number
                        '%s' => 'test/string/string', // http://www.ru/test/any_text/any_text
                        'value' => 'test/string/value',
                        'crazy' =>
                            [
                                'long' =>
                                    [
                                        '%d' =>
                                            [
                                                '%s' => [
                                                    'url' => 'crazy/long/url',
                                                ]
                                            ]
                                    ]
                            ]
                    ]
                ]
            ]
        );

        $this->assertEquals('index_page', $router->route(''));
        $callback = $router->route('callback');
        /**
         * @var callable $callback
         */
        $this->assertEquals('HELLO', $callback());
        $this->assertEquals('single_integer', $router->route('/100'));
        $this->assertEquals('single_string', $router->route('/string'));
        $this->assertEquals('test/string', $router->route('/test/string'));
        $this->assertEquals('test/string/integer', $router->route('/test/string/100500'));
        $this->assertEquals('test/string/string', $router->route('/test/string/some_string'));
        $this->assertEquals('test/string/value', $router->route('/test/string/value'));
        $this->assertEquals('crazy/long/url', $router->route('/test/string/crazy/long/100/hello/url/'));
    }

    /**
     * @throws Redirect
     * @throws \Plumbus\Exception\NotFound
     */
    public function testAddRoutingRuleByArray()
    {
        $router = new Router();

        $router->addRoutingRule(['%s' => 'some_page']);
        $this->assertEquals('some_page', $router->route('/blabla'));

        $router->addRoutingRule(['%s' => 'another_page']);
        $this->assertEquals('another_page', $router->route('/blabla'));

        $router->addRoutingRule(['%s=var1' => ['%d=var2' => 'varvar']]);
        $variables = [];
        $this->assertEquals('varvar', $router->route('/bla/100500', $variables));
        $this->assertEquals('bla', $variables['var1']);
        $this->assertEquals('100500', $variables['var2']);
    }

    public function testAddRoutingRuleByString()
    {
        $router = new Router();

        $router->addRoutingRule('/%s/%s/%s', 'some_page1');
        $this->assertEquals('some_page1', $router->route('/bla/bla/bla'));

        $router->addRoutingRule('/%s/', 'some_page2');
        $this->assertEquals('some_page2', $router->route('/blabla'));

        $router->addRoutingRule('%s', 'some_page_string');
        $this->assertEquals('some_page_string', $router->route('/blabla'));

        $router = new Router();

        $router->addRoutingRule('%d', 'some_page_integer');
        $this->assertEquals('some_page_integer', $router->route('/100500'));

        $router->addRoutingRule(['%s=var1' => ['%d=var2' => 'varvar']]);
        $router->addRoutingRule('/%s=var1/%d=var2', 'varvar');
        $variables = [];
        $this->assertEquals('varvar', $router->route('/bla/100500', $variables));
        $this->assertEquals('bla', $variables['var1']);
        $this->assertEquals('100500', $variables['var2']);
    }

    /**
     * @throws Redirect
     * @throws \Plumbus\Exception\NotFound
     * @throws \Plumbus\Exception\Runtime
     */
    public function testRouterRule()
    {
        $router = new Router();
        $router->setTrailingSlashBehavior(
            Router::TRAILING_SLASH_BEHAVIOR_IGNORE
        );
        /**
         * set routing for rule "DELETE"
         */
        $router->setRoutingMap(
            [
                'item' => [
                    '%s' => [
                        '%d' => 'DELETE_ITEM',
                    ]
                ]
            ],
            'DELETE'
        );
        /**
         * set routing for rule "PUT"
         */
        $router->setRoutingMap(
            [
                'item' => [
                    '%s' => [
                        '%d' => 'PUT_ITEM',
                    ]
                ]
            ],
            'PUT'
        );

        /**
         * resolve map by DELETE rule
         */
        $variables = [];
        $this->assertEquals('DELETE_ITEM', $router->route('/item/string/100500', $variables, 'DELETE'));
        $this->assertEquals('PUT_ITEM', $router->route('/item/string/100500', $variables, 'PUT'));
    }

    public function testRouterVariables()
    {
        $router = new Router();
        $router->setTrailingSlashBehavior(
            Router::TRAILING_SLASH_BEHAVIOR_IGNORE
        );
        /**
         * set routing for rule "DELETE"
         */
        $router->setRoutingMap(
            [
                'item' => [
                    '%s=var1' => [
                        '%s=var2' => 'DELETE_ITEM',
                    ]
                ]
            ],
            'DELETE'
        );

        /**
         * set routing for rule "PUT"
         */
        $router->setRoutingMap(
            [
                'item' => [
                    '%s' => [
                        '%d=myCustomVariable3' => 'PUT_ITEM',
                    ]
                ]
            ],
            'PUT'
        );

        /**
         * resolve map by DELETE rule
         */
        $variables = [];
        $this->assertEquals('DELETE_ITEM', $router->route('/item/value1/value2', $variables, 'DELETE'));

        /**
         * resolve map by PUT rule
         */
        $this->assertEquals('PUT_ITEM', $router->route('/item/loasdll/100000', $variables, 'PUT'));

        /**
         * checking variables
         */
        $this->assertEquals('value1', $variables['var1']);
        $this->assertEquals('value2', $variables['var2']);
        $this->assertEquals('100000', $variables['myCustomVariable3']);
    }
}
