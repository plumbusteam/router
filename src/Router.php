<?php namespace Plumbus;

use Plumbus\Exception\NotFound;
use Plumbus\Exception\Redirect;
use Plumbus\Exception\Runtime;

class Router
{
    const TRAILING_SLASH_BEHAVIOR_REDIRECT_IF_EXISTS = 1;
    const TRAILING_SLASH_BEHAVIOR_REDIRECT_IF_NOT_EXISTS = 2;
    const TRAILING_SLASH_BEHAVIOR_IGNORE = 3;

    const DEFAULT_REQUEST_TYPE_NAME = 'GET';
    /**
     * @var array
     */
    private $routingMapRules;

    /**
     * @var array
     */
    private $trailingSlashBehaviors;

    /**
     * @param array $routingMap
     * @param string $requestTypeName
     * @return $this
     */
    public function setRoutingMap(array $routingMap, string $requestTypeName = self::DEFAULT_REQUEST_TYPE_NAME)
    {
        $this->routingMapRules[$requestTypeName] = $routingMap;
        return $this;
    }

    /**
     * @param string $requestTypeName
     * @return mixed
     */
    public function getMap(string $requestTypeName = self::DEFAULT_REQUEST_TYPE_NAME)
    {
        return $this->routingMapRules[$requestTypeName];
    }

    /**
     * @param $rule
     * @param null $ruleValue
     * @param string $requestTypeName
     * @return $this
     */
    public function addRoutingRule($rule, $ruleValue = null, string $requestTypeName = self::DEFAULT_REQUEST_TYPE_NAME)
    {
        if (is_string($rule)) {
            $rule = $this->makeRuleFromString($rule, $ruleValue);
        }

        if (is_array($rule) && !empty($rule)) {
            $ruleKey = array_keys($rule)[0];
            list($cleanRuleKey, $variableName) = $this->extractRuleKeyAndVariableName($ruleKey);
            if (isset($this->routingMapRules[$requestTypeName][$cleanRuleKey]) && in_array($cleanRuleKey,
                    ['%s', '%d'])
            ) {
                unset($this->routingMapRules[$requestTypeName][$cleanRuleKey]);
            }
            $this->routingMapRules[$requestTypeName][$ruleKey] = $rule[$ruleKey];
        }
        return $this;
    }

    private function makeRuleFromString(string $rule, $ruleValue)
    {
        $ruleParts = explode("/", $rule);
        $ruleArray = [];
        $this->appendValue($ruleArray, $ruleParts, $ruleValue);
        return $ruleArray;
    }

    /**
     * @param array $array
     * @param $values
     * @param $ruleValue
     * @return array
     */
    private function appendValue(array &$array, &$values, $ruleValue)
    {
        $value = null;
        while (!empty($values) && !($value)) {
            $value = array_shift($values);
        }

        if ($value) {
            $array[$value] = $this->appendValue($array, $values, $ruleValue);
        }
        return !empty($array) ? $array : $ruleValue;
    }

    /**
     * @param string $ruleKey
     * @return array
     */
    private function extractRuleKeyAndVariableName(string $ruleKey)
    {
        $variableName = null;
        if (!empty($ruleKey[0]) && '%' == $ruleKey[0] && strlen($ruleKey) > 3) {
            $variableName = substr($ruleKey, 3);
            $ruleKey = substr($ruleKey, 0, 2);
        }
        return [$ruleKey, $variableName];
    }

    /**
     * @param int $behavior
     */
    public function setTrailingSlashBehavior(int $behavior, string $requestTypeName = self::DEFAULT_REQUEST_TYPE_NAME)
    {
        $this->trailingSlashBehaviors[$requestTypeName] = $behavior;
    }

    /**
     * @param string $relativeUri
     * @param array $variables
     * @param string $requestTypeName
     * @return array|callable|string
     * @throws NotFound
     * @throws Redirect
     */
    public function route(
        string $relativeUri,
        array &$variables = [],
        string $requestTypeName = self::DEFAULT_REQUEST_TYPE_NAME
    ) {
        if (empty($this->routingMapRules[$requestTypeName])) {
            throw new Runtime('use setRoutingMap() before calling "route" method');
        }

        if (!empty($this->trailingSlashBehaviors[$requestTypeName]) && $this->trailingSlashBehaviors[$requestTypeName] !== self::TRAILING_SLASH_BEHAVIOR_IGNORE) {
            $this->resolveTrailngSlashBehavior($relativeUri, $requestTypeName);
        }

        $idealRequestUri = $this->getIdealRequestUri($relativeUri);

        if ($idealRequestUri !== $relativeUri) {
            $error = new Redirect($relativeUri . ' <> ' . $idealRequestUri);
            $error->setRedirectUri($idealRequestUri);
            throw $error;
        }

        $urlArray = explode('?', $relativeUri);
        $uri = $urlArray[0];


        $requestUriParts = explode('/', $uri);
        $siteMap = $this->routingMapRules[$requestTypeName];
        $siteMap = $this->getPageIdByRequestArray($siteMap, $requestUriParts, $variables);

        if (empty($siteMap) || is_array($siteMap)) {
            throw new NotFound('"' . $relativeUri . '" not found');
        }

        return $siteMap;
    }

    /**
     * @param string $relativeUri
     * @param string $requestTypeName
     * @throws Redirect
     */
    private function resolveTrailngSlashBehavior(
        string $relativeUri,
        string $requestTypeName = self::DEFAULT_REQUEST_TYPE_NAME
    ) {
        $behavior = $this->trailingSlashBehaviors[$requestTypeName] ?? self::TRAILING_SLASH_BEHAVIOR_IGNORE;
        if (mb_strlen($relativeUri) > 1) {
            if (mb_substr($relativeUri, -1) == '/') {
                if ($behavior === self::TRAILING_SLASH_BEHAVIOR_REDIRECT_IF_EXISTS) {
                    $error = new Redirect();
                    $error->setRedirectUri(mb_substr($relativeUri, 0, mb_strlen($relativeUri) - 1));
                    throw $error;
                }
            } else {
                if ($behavior === self::TRAILING_SLASH_BEHAVIOR_REDIRECT_IF_NOT_EXISTS) {
                    $error = new Redirect();
                    $error->setRedirectUri($relativeUri . '/');
                    throw $error;
                }
            }
        }
    }

    /**
     * @param string $relativeUri
     * @return mixed
     */
    private function getIdealRequestUri(string $relativeUri)
    {
        return preg_replace('/[\/]+/', '/', $relativeUri);
    }

    /**
     * @param array|string $siteMap
     * @param array $requestUriParts
     * @param array|null $variables
     * @return array|string|callable
     */


    private function getPageIdByRequestArray(&$siteMap, array &$requestUriParts, array &$variables = [])
    {
        if (!is_array($siteMap)) {
            return $siteMap;
        }
        $currentRequestUriPart = '';
        while (empty($currentRequestUriPart) && !empty($requestUriParts)) {
            $currentRequestUriPart = array_shift($requestUriParts);
        }

        if (isset($siteMap[$currentRequestUriPart])) {
            return $this->getPageIdByRequestArray($siteMap[$currentRequestUriPart], $requestUriParts, $variables);
        }

        foreach ($siteMap as $siteMapId => $siteMapPart) {
            $variableName = null;
            list($ruleKey, $variableName) = $this->extractRuleKeyAndVariableName($siteMapId);
            if (!$ruleKey) {
                $ruleKey = $siteMapId;
            }

            if (
                ($ruleKey === '%d') && (intval($currentRequestUriPart) . '' === $currentRequestUriPart)
                ||
                ($ruleKey === '%s' && $currentRequestUriPart != '')
            ) {
                if ($variableName) {
                    $variables[$variableName] = $currentRequestUriPart;
                }
                return $this->getPageIdByRequestArray($siteMap[$siteMapId], $requestUriParts, $variables);
            }
        }
    }
}