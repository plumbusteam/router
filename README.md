# beeapp/router #
## Лёгкий и быстрый роутинг запросов на PHP ##

### Для чего служит? ###

Простой роутер, сопоставляющий некоторый путь в URL с некоторым значением. Например, нам нужно сопоставить путь
/news/100/edit со значением "news_edit", которое является ключом запрашиваемой страницы в конфиге контроллеров.
Роутер пользуется "картой роутинга" - массивом, ключи которого обозначают часть URL и могут содержать значения:

*	пустое значение - именно эта страница,
*	%s - любая строка
*	%d - любое число
*	любая строка - именно эта строка.

При этом значения массива - это или строки, или callback функции, которые будут вызваны в случае успешного разрешения роутинга.
Для того, чтобы части URL запроса были преобразованы в переменные, можно указывать имя переменной через знак
равенства рядом с ключом роутинга - например, '%s=some_variable' указывает на то, что эта чать URL является любой строкой
и должна быть доступна как переменная с именем some_variable.

### Установка ###

Для установки, воспользуйтесь конфигом composer'а - пропишите в секции require файла composer.json:
```"beeapp/application" : "dev-master"```

Для проверки работоспособности, можно запустить unit-тестирование командой
```
# vendor/phpunit/phpunit/phpunit --color src/
PHPUnit 4.8.21-5-gacbb9e4 by Sebastian Bergmann and contributors.

.......

Time: 598 ms, Memory: 4.00Mb

OK (7 tests, 22 assertions)
```

### Использование  ###

#### Пример 1 ####

В Этом примере мы задаем 2 роутинга - для методов PUT и DELETE.
Затем выполняем роутинг 2 раза - для обоих методов.
Нектороые части пути содержат некоторые значения после %s или %d - значит, они попадут
как переменные в массив variables и будут содержать конкретные значения.

```
   $router = new Router();

   $router->setTrailingSlashBehavior(
       Router::TRAILING_SLASH_BEHAVIOR_IGNORE
   );
   /**
    * set routing for rule "DELETE"
    */
   $router->setRoutingMap(
       [
           'item' => [
               '%s=var1' => [
                   '%s=var2' => 'DELETE_ITEM',
               ]
           ]
       ],
       'DELETE'
   );

   /**
    * set routing for rule "PUT"
    */
   $router->setRoutingMap(
       [
           'item' => [
               '%s' => [
                   '%d=myCustomVariable3' => 'PUT_ITEM',
               ]
           ]
       ],
       'PUT'
   );

   /**
    * resolve map by DELETE rule
    */
   $variables = [];
   $this->assertEquals('DELETE_ITEM', $router->route('/item/value1/value2', 'DELETE', $variables));

   /**
    * resolve map by PUT rule
    */
   $this->assertEquals('PUT_ITEM', $router->route('/item/loasdll/100000', 'PUT', $variables));

   /**
    * checking variables
    */
   $this->assertEquals('value1', $variables['var1']);
   $this->assertEquals('value2', $variables['var2']);
   $this->assertEquals('100000', $variables['myCustomVariable3']);
```

#### Пример 2 ####

Живой пример - есть контроллер для отрисовки поста автора и такой роутинг к нему:
```
    $router = new Router();
    $router->setTrailingSlashBehavior(
        Router::TRAILING_SLASH_BEHAVIOR_IGNORE
    );
    /**
     * set routing for rule "GET"
     */
    $router->setRoutingMap(
        [
            'author' => [
                '%s=authorName' => [
                    '%d=postId' => 'author_post',
                ]
            ]
        ],
        'GET'
    );
    $variables = [];
    $controllerName = $router->route('/author/tema/100500', 'GET', $variables);
    echo 'controller:' . $controllerName . "\nvariables:\n";
    print_r($variables);
```

Вывод:

```
    controller:author_post
    variables:
    Array
    (
        [authorName] => tema
        [postId] => 100500
    )
```

#### Пример 3 ####

Добавление правил роутинга с помощью массивов или строк:
```
    $router = new Router();

    $router->addRoutingRule(['%s=var1' => ['%d=var2' => 'varvar']]);
    $variables = [];
    $this->assertEquals('varvar', $router->route('/bla/100500', $variables));
    $this->assertEquals('bla', $variables['var1']);
    $this->assertEquals('100500', $variables['var2']);

    $router->addRoutingRule('/%s/%s/%s', 'some_page1');
    $this->assertEquals('some_page1', $router->route('/bla/bla/bla'));
```

### ПОМОЩЬ И ПОДДЕРЖКА ###
you can send any questions to amuhc@yandex.ru